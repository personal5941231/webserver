#include <iostream>
#include <string.h>
#include <sstream>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/epoll.h>
#include <vector>
#include <thread>


using namespace std;

#define BUFFER_LENGTH 2048
#define LISTEN_PORT 2048

enum MESSAGE_TYPE {
	TXT,
	JSON,
	OBJECT,
	ARRAY,
};

 enum EVENTS_TCP{
	WRITE,
	READ,
};

typedef struct HREADER {
	MESSAGE_TYPE type;
	unsigned int length ;
	std::string send_info ;
	std::string recive_info;
}HREADER_T;

typedef struct RSData {
	HREADER_T header;
	std::string info;
}RSData_T;

struct ConnData {
	int connfd;
	int epfd;
};


void readThread(int connfd, int epfd) {
	char buffer[BUFFER_LENGTH] = { 0 };
	int n = recv(connfd, buffer, BUFFER_LENGTH, 0);
	if (n > 0) {
		printf("recv:%s\n", buffer);
		send(connfd, buffer, n, 0);
	}
	else if (n == 0) {
		printf("close\n");
		epoll_ctl(epfd, EPOLL_CTL_DEL, connfd, NULL);
		close(connfd);
	}
}

std::string structTOstring(RSData &data){
	std::stringstream ss;
    ss << "length:" << data.header.length << "\n";
    ss << "recive_info:" << data.header.recive_info << "\n";
    ss << "send_info:" << data.header.send_info << "\n";
	ss << "data:"<<data.info<<"\n";
	std::cout<<"ss:"<<ss.str()<<std::endl;
    return ss.str();
}

void accessTxt(char* buf, ConnData connData) {
	printf("accessTxt\n");
	if (!buf) {
		printf("send buf is null.");
		return;
	}
	int len = sizeof(buf);
	MESSAGE_TYPE type = MESSAGE_TYPE::TXT;
	RSData_T data ;	
	data.header.recive_info = std::string("empty");
	data.header.send_info = std::string("123.45.13.1");
	data.info = buf;
	data.header.length = data.info.length();
	std::string str = structTOstring(data);
	
	if (len <= BUFFER_LENGTH) {		
		send(connData.connfd, str.c_str(), str.length(), 0);
		epoll_ctl(connData.epfd, EPOLL_CTL_DEL, connData.connfd, NULL);
		close(connData.connfd);
	
		return ;
	}
	if (len / BUFFER_LENGTH < 4) {
		int yushu = len % BUFFER_LENGTH;
		int count = (len - yushu) / BUFFER_LENGTH;
		for (int i = 0; i < count; i++) {
			send(connData.connfd, str.substr(i,BUFFER_LENGTH*i-1).c_str(),BUFFER_LENGTH*i,0);
		}
		if (yushu != 0) {
			send(connData.connfd, str.substr(BUFFER_LENGTH*count, len-1).c_str(), yushu, 0);
		}
		epoll_ctl(connData.epfd, EPOLL_CTL_DEL, connData.connfd, NULL);
	}
	else {
		///...
		epoll_ctl(connData.epfd, EPOLL_CTL_DEL, connData.connfd, NULL);
	}

}

void accessArray(char* buf) {

}

void accessObject(char* buf) {

}

void accessJson(char* buf) {

}

void sendMessage(MESSAGE_TYPE type, char* buf,ConnData data) {
	printf("sendMessage\n");
	switch (type) {
	case MESSAGE_TYPE::TXT:
		accessTxt(buf, data);
		break;
	case MESSAGE_TYPE::ARRAY:
		break;
	case MESSAGE_TYPE::OBJECT:
		break;
	case MESSAGE_TYPE::JSON:
		break;
	}
}

void manager_accept(struct epoll_event &ev){
	
}

//写
void write(int connfd){
	
}
//读
void read(int connfd){
	char* buffer[8];
	memset(buffer,0,sizeof(buffer));
	printf("32323:connfd:%d\n",connfd);
	//当socket为堵塞模式时若未读取到指定长度的数据，则recv会一直堵塞
	ssize_t re = recv(connfd,buffer,8,0);
	printf("323233234234::%d\n",re);
	if(re<=0){
		printf("%d connect already is close.\n",connfd);
	}else{
		printf("read data:%s",buffer);
	}
	
}

//注册事件
void registerEvent(EVENTS_TCP type,int connfd){
	switch(type){
		case EVENTS_TCP::WRITE:
		write(connfd);
		break;
		case EVENTS_TCP::READ:
		read(connfd);
		break;
	}
}

int main(void) {

	//创建套接字
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in servadrr;
	//清理内存
	memset(&servadrr, 0, sizeof(struct sockaddr_in));
	servadrr.sin_family = AF_INET;
	servadrr.sin_addr.s_addr = htonl(INADDR_ANY);
	servadrr.sin_port = htons(LISTEN_PORT);
	//绑定端口
	if (-1 == bind(sockfd, (struct sockaddr*)&servadrr, sizeof(struct sockaddr))) {
		printf("bind failed: %s", strerror(errno));
		return -1;
	}
	//开始监听
	listen(sockfd, 10);

	//创建epoll实例
	int epfd = epoll_create(1);
	struct epoll_event ev;
	//监听读事件并且设置边缘触发
	ev.events = EPOLLIN | EPOLLET;
	ev.data.fd = sockfd;

	epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &ev);
	struct epoll_event events[1024] = { 0 };
	struct sockaddr_in client_addr;
	socklen_t len = sizeof(client_addr);
	printf("start work...\n哈哈哈\n");
	while (1) {
		int nready = epoll_wait(epfd, events, 1024, -1);
		if (nready < 0) {
			continue;
		}
		for (int i = 0; i < nready; i++) {
			int connfd = events[i].data.fd;
			if (sockfd == connfd) {
				int clientfd = accept(sockfd, (struct sockaddr*)&client_addr, &len);
				if (clientfd <= 0) {
					continue;
				}
				printf("client fd:%d\n", clientfd);
				ev.events = EPOLLIN;
				ev.data.fd = clientfd;
				epoll_ctl(epfd, EPOLL_CTL_ADD, clientfd, &ev);
			}
			else if (events[i].events & EPOLLIN) {			
				char buffer[BUFFER_LENGTH] = { 0 };
				int n = recv(connfd,buffer,BUFFER_LENGTH,0);
				if(n>0){
					ConnData data;
					data.connfd = connfd;
					data.epfd = epfd;
					//sendMessage(MESSAGE_TYPE::TXT, buffer,data);					
					registerEvent(EVENTS_TCP::READ, connfd);
				}else if( n == 0)
				{
					printf("close\n");
					epoll_ctl(epfd, EPOLL_CTL_DEL, connfd, NULL);
					close(connfd);
				}
				printf("recive connected.");			
				//std::thread* thread = new std::thread(readThread, connfd, epfd);
			}
		}
	}
	
}